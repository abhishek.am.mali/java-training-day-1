class Constructor_Example {
    private int id;
    private String name;
    public Constructor_Example() {
        id = 10; // Some default ID
        name = "Abhishek Mali"; // Default Name
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }
}

public class KT_Day1_Q1 {
    public static void main(String[] args) {
        String question, answer;

        question = "1. What is a constructor? Explain by example.";
        System.out.println(question);

        answer = "\n-> A constructor is a method in class which has the same name as the class in which it was created in.\nConstructor is a member function which is used to initialize an object while creating it.\n";
        System.out.println(answer);

        System.out.println("Example:-");
        System.out.println("A class named \"Constructor_Example\" is created above in this program and a function with same name is created inside it.");

        Constructor_Example obj = new Constructor_Example();

        System.out.println("\nPrinting value of variables \"id\" and \"name\" even before setting them does not give an error.\nJust because values have been initialized as soon as the object of class \"Constructor_Example\" was created.");
        System.out.println("\tID = " + obj.getId());
        System.out.println("\tName = " + obj.getName());
    }
}

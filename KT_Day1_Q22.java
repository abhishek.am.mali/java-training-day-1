/*
Q 22 Write a Java program to get specific files by extensions from a specified folder.
*/

import java.io.File;

public class KT_Day1_Q22 {
    public static void main(String[] args) {
        File file = new File("E:\\Programs\\C,C++ Programs");
        String[] fileList = file.list();

        for(String name:fileList) {
            if (name.toLowerCase().endsWith(".cpp"))
                System.out.println(name);
        }
    }
}

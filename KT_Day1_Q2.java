class Default_Constructor_Example {
    private int id;
    private String name;

    // This is a default constructor
    public Default_Constructor_Example() {
        System.out.println("x---------Default Constructor called----------x");
        id = 10;
        name = "Abhishek Mali";
    }

    // This is a parameterized constructor
    public Default_Constructor_Example(int id, String myName) {
        System.out.println("x----------Parameterized Constructor called---------x");
        this.id = id;
        this.name = myName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }
}

public class KT_Day1_Q2 {
    public static void main(String[] args) {
        System.out.println("2. What is the default constructor ? Explain by example.");
        System.out.println("\n-> Default constructor is a constructor which either has no parameters. OR\nIf it has parameters then all the parameters are initialized with a default value");
        // Creating object of class Default_Constructor_Example

        System.out.println("\nDefault constructor will be called as soon as below object is created");
        Default_Constructor_Example obj1 = new Default_Constructor_Example();

        System.out.println("\nParameterized constructor will be called by below line.");
        Default_Constructor_Example obj2 = new Default_Constructor_Example(20, "PK");
    }
}

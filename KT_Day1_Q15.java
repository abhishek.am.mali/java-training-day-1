// Abstract class
abstract class Car {
    abstract void accelerate();
}
class SuperCar extends Car {
    void accelerate() {
        System.out.println("Running at 100 MPH.");
    }
}

public class KT_Day1_Q15 {
    public static void main(String[] args) {
        System.out.println("15. What is a Java Abstract Class ? [Explain with Example]");
    }
}

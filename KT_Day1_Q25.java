/*
Q 25  Write a Java program to check if given pathname is a directory or a file.
*/

import java.io.File;

public class KT_Day1_Q25 {
    public static void main(String[] args) {
        File file = new File("D:/");
        String[] fileList = file.list();

        if (file.isFile()) {
            System.out.println(file.getAbsolutePath() + "Is a file.");
        }
        else if (file.isDirectory()) {
            System.out.println(file.getAbsolutePath() + "Is a folder");
        }
        else {
            System.out.println(file.getAbsolutePath() + "Is neither a file or a folder");
        }
    }
}

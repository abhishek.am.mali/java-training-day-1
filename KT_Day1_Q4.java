public class KT_Day1_Q4 {

    static int add(int a, int b) {
        return a + b;
    }

    static float add(float a, float b) {
        return a + b;
    }

    public static void main(String[] args) {
        System.out.println("4. What is Polymorphism? Explain by example.");
        System.out.println("-> The word polymorphism is derived from two different words Poly means many and Morphism means forms.");
        System.out.println("Is can be defined as the ability of an object to  take on many forms.");

        System.out.println("Two different methods with same name are called with different parameters.\nIn this way, two different functions are called");
        System.out.println(add(3, 4));
        System.out.println(add(3.6f, 5.9f));
    }
}

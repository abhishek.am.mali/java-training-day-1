class Student {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

public class KT_Day1_Q7 {
    public static void main(String[] args) {
        System.out.println("7. Explain the concept of Encapsulation with example/program");

        System.out.println("\nEncapsulation is defined as wrapping up data into a single unit. In simple terms Sensitive data should be hidden from users");
        System.out.println("A class can be said as fully encapsulated if all it's data members are private. And than getters and setters are made for it's data members according to the requirement.");
        Student obj = new Student();

        System.out.println(obj.getId());
        obj.setId(35);
        System.out.println(obj.getId());
    }
}

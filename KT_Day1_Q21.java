/*
Q 21 Write a Java program to get a list of all file/directory names from the given.
*/

import java.io.File;

public class KT_Day1_Q21 {
    public static void main(String[] args) {
        File file = new File("D:/");
        String[] fileList = file.list();

        for(String name:fileList) {
            System.out.println(name);
        }
    }
}

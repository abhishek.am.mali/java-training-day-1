public class KT_Day1_Q12 {
    public static void main(String[] args) {
        System.out.println("12. Explain how the keyword transient works. What fields would you mark as transient in a class ? [With Example]");
        System.out.println("The transient keyword in Java is used to indicate that a field should not be part of the serialization.");
    }
}

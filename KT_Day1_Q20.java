/*
SOLID principles consist of 5 main concepts:-

S - Single Responsibility
    Any class created should have just one responsibility
    It should have only one reason to change.

O - Open Close
    This concept indicates that any class should be open for extension and closed for modification.

L - Liskov Substitution
    Liskov substitution indicates that if a class A is a subtype of class B then we should be able to replace B with A, without disrupting the behavior of program.

I - Interface Segregation
    This indicates that larger interfaces should be broken down into smaller ones.

D - Dependency Inversion
    It refers to decoupling of software modules.
*/

public class KT_Day1_Q20 {
    public static void main(String[] args) {
        System.out.println("What are the SOLID principles and how do they apply to Java ? [Explain with Example]");
    }
}
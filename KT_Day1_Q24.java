/*
Q 24 Write a Java program to check if a file or directory has read and write permission.
*/

import java.io.File;

public class KT_Day1_Q24 {
    public static void main(String[] args) {
        File file = new File("E:\\Programs\\C,C++ Programs\\Two GP.cpp");
        String[] fileList = file.list();

        if (file.canWrite()) {
            System.out.println(file.getAbsolutePath() + " can write.\n");
        }
        else {
            System.out.println(file.getAbsolutePath() + " cannot write.\n");
        }
        if (file.canRead()) {
            System.out.println(file.getAbsolutePath() + " can read.\n");
        }
        else {
            System.out.println(file.getAbsolutePath() + " cannot read.\n");
        }
    }
}

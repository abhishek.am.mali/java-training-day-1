class Base {
    public void fun1() {
        System.out.println("Function 1 of Base class called.");
    }
}
class Derived extends Base {
    public void fun2() {
        System.out.println("Function 2 of derived class called.");
    }
    public void fun1() {
        System.out.println("Function 1 of derived class called.");
    }
}

public class KT_Day1_Q5 {
    public static void main(String[] args) {
        System.out.println("5. What is Overloading ? What is Overriding ? Explain by example.");
        System.out.println("Overloading - When two different functions of a same class, having same name perform different function then it ic called as Overloading.");
        System.out.println("Overriding - When two different functions, one of a child class and another of the parent class, having same name perform different function then it ic called as Overloading.");

        Base b = new Base();
        Derived d = new Derived();
        // Function 1 of base class called
        b.fun1();

        // Overridden Function (Function 1) of derived class called
        d.fun1();

        // Function 2 of derived class called
        d.fun2();
    }
}

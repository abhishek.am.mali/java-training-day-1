class Parent1 {
    public void fun1() {
        System.out.println("Function 1 of Parent class called.");
    }

    /*
    Declared below is a static method which cannot be overridden.
    Below code is commented out because it will cause an error.
    public static void fun1() {
        System.out.println("Function 1 of Parent class called.");
    }
     */
}
class Child1 extends Parent1 {
    public void fun2() {
        System.out.println("Function 2 of derived class called.");
    }
    public void fun1() {
        System.out.println("Function 1 of derived class called.");
    }
}

public class KT_Day1_Q6 {
    public static void main(String[] args) {
        System.out.println("6. Can a static method be overwritten ? Explain by example.");
        System.out.println("\nA static method cannot be overridden in java");
        System.out.println("Method overriding is done at runtime and is based on dynamic binding but a static method is bonded using static binding at compile time.");

        Parent1 p = new Parent1();
        Child1 c = new Child1();
        // Function 1 of Parent1 class called
        p.fun1();

        // Overridden Function (Function 1) of Child1 class called
        c.fun1();
    }
}

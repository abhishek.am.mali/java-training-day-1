/*
Q 23 Write a Java program to check if a file or directory specified by pathname exists or not.
 */

import java.io.File;

public class KT_Day1_Q23 {
    public static void main(String[] args) {
        File file = new File("G:/");
        String[] fileList = file.list();

        if (file.exists()) {
            System.out.println("The Folder or file exists.");
        }
        else {
            System.out.println("The Folder or file does not exist.");
        }
    }
}

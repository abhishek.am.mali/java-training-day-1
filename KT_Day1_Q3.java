class Parent {
    int x;
    public Parent() {
        System.out.println("Parent class constructor called.");
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        System.out.println("Value os X is being set.");
        this.x = x;
    }
}

class Child extends Parent {
    int y;

    public Child() {
        System.out.println("Child class constructor called.");
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        System.out.println("Value os Y is being set.");
        this.y = y;
    }
}

public class KT_Day1_Q3 {
    public static void main(String[] args) {
        System.out.println("3. Explain the concept of Inheritance. Why doesn't Java supports the concept of Multiple Inheritance ? Explain by example.");
        System.out.println("\n-> Inheritance is used to import the properties of parent class to the child class.");
        System.out.println("An example of simple inheritance is implemented in above program");

        // Creating object of parent class
        Parent p = new Parent();
        p.setX(12);
        System.out.println(p.getX());

        // Creating object of child class
        Child c = new Child();
        c.setY(18);
        System.out.println(c.getY());

        // Using child class object to perform operations on data members and member functions of parent class.
        c.setX(56);
        System.out.println(c.getX());

        System.out.println("\nJava Supports only 3 types of inheritance which are:-\n\t1. Single\n\t2. Multilevel\n\t3. Hierarchical");

        System.out.println("\nJava doesn't support multiple inheritance because:-");
        System.out.println("Every class is the child of Object class. When it inherits from more than one parent class, sub class gets the ambiguity to acquire the property of Object class.");
        System.out.println("Every class has a constructor whether we write it explicitly or not. When we call super() to call the constructor of Parent class, if the class has more than one Parent class it gets confused.");

    }
}

final class Constant {
    private final double pi = 3.14;

    public double getPi() {
        return pi;
    }
}

public class KT_Day1_Q9 {
    public static void main(String[] args) {
        System.out.println("9. How can we write an Immutable class ?");
        System.out.println("\nIf a class is immutable it means that once an object is created it's contents cannot be changed.");
        System.out.println("The class must be declared as final, such that it's child classes can't be created.");
        System.out.println("Data members must be declared as private such that direct access is not possible.");
        System.out.println("Data members must be declared as final such that it's value can't be changed after object creation.");

        Constant obj = new Constant();
        System.out.println(obj.getPi());
    }
}

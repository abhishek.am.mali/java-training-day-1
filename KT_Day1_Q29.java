/*
Q 29 Write a Java program to get file size in bytes, kb, mb.
 */
import java.io.File;


public class KT_Day1_Q29 {
    private static double fileSizeInBytes(File file) {
        return file.length();
    }

    private static double fileSizeInKB(File file) {
        return fileSizeInBytes(file)/1024;
    }

    private static double fileSizeInMB(File file) {
        return fileSizeInKB(file)/1024;
    }

    public static void main(String[] args) {
        File file = new File("E:\\Tutorials\\Learn CSS in 20 Minutes.mp4");
        if (file.exists()) {
            System.out.println("Size in Bytes = " + fileSizeInBytes(file) + " Bytes");
            System.out.println("Size in KB = " + fileSizeInKB(file) + " KB");
            System.out.println("Size in MB = " + fileSizeInMB(file) + " MB");
        }
    }
}

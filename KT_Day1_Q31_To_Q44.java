import java.util.*;

public class KT_Day1_Q31_To_Q44 {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<Employee>();


        employees.add(new Employee(111, "Jiya Brein", 32, "Female", "HR", 2011, 25000.0));
        employees.add(new Employee(122, "Kaushal Jani", 25, "Male", "Sales And Marketing", 2015, 13500.0));
        employees.add(new Employee(133, "Harshil Nagar", 29, "Male", "Infrastructure", 2012, 18000.0));
        employees.add(new Employee(144, "Raj Darbar", 28, "Male", "Product Development", 2014, 32500.0));
        employees.add(new Employee(155, "Nima Roy", 27, "Female", "HR", 2013, 22700.0));
        employees.add(new Employee(166, "Sid Barot", 43, "Male", "Security And Transport", 2016, 10500.0));
        employees.add(new Employee(177, "Pruthvi Soni", 35, "Male", "Account And Finance", 2010, 27000.0));
        employees.add(new Employee(188, "Parth Dabgar", 31, "Male", "Product Development", 2015, 34500.0));
        employees.add(new Employee(199, "Amelia Zoe", 24, "Female", "Sales And Marketing", 2016, 11500.0));
        employees.add(new Employee(200, "Ashish Patel", 38, "Male", "Security And Transport", 2015, 11000.5));
        employees.add(new Employee(211, "Jasna Kaur", 27, "Female", "Infrastructure", 2014, 15700.0));
        employees.add(new Employee(222, "Aaalap Patel", 25, "Male", "Product Development", 2016, 28200.0));
        employees.add(new Employee(233, "Jyothi Reddy", 27, "Female", "Account And Finance", 2013, 21300.0));
        employees.add(new Employee(244, "Divyesh Solanki", 24, "Male", "Sales And Marketing", 2017, 10700.5));
        employees.add(new Employee(255, "Jay Solanki", 23, "Male", "Infrastructure", 2018, 12700.0));
        employees.add(new Employee(266, "Sanvi Pandey", 26, "Female", "Product Development", 2015, 28900.0));
        employees.add(new Employee(277, "Kishan Panchal", 31, "Male", "Product Development", 2012, 35700.0));


        Iterator itr = employees.iterator();
        while (itr.hasNext()) {
            Employee e = (Employee)itr.next();
            System.out.println(e.getAge());
        }


        for (int i = 0; i < employees.size(); i++) {
            System.out.println(employees.get(i));
        }

        for (Employee person: employees) {
            // System.out.println(person);
            Employee emp = (Employee)person;
            System.out.println(emp.getName());
        }

        // Q 31
        // How many male and female employees are there in the organization?
        System.out.println("\nQuestion 31:-");

        int males = 0, females = 0;
        for (Employee person: employees) {
            // System.out.println(person);
            Employee emp = (Employee)person;

            if (emp.getGender() == "Male") {
                males += 1;
            }
            else if (emp.getGender() == "Female"){
                females += 1;
            }
        }

        System.out.println("Number of Male employees = " + males);
        System.out.println("Number of Female employees = " + females);



        // Q 32
        // Print the name of all departments in the organization?
        System.out.println("\nQuestion 32:-");

        Set<String> departments = new HashSet<String>();

        for (Employee person: employees) {
            // System.out.println(person);
            Employee emp = (Employee)person;
            departments.add(emp.getDepartment());
        }

        System.out.println("Available departments are: ");
        System.out.println(departments);



        // Q 33
        // What is the average age of male and female employees?
        System.out.println("\nQuestion 33:-");

        int malesAge = 0, femalesAge = 0;
        for (Employee person: employees) {
            Employee emp = (Employee)person;

            if (emp.getGender() == "Male") {
                malesAge += emp.getAge();
            }
            else if (emp.getGender() == "Female"){
                femalesAge += emp.getAge();
            }
        }

        System.out.println("Average Age of Male employees = " + (double)malesAge/males);
        System.out.println("Average Age of Female employees = " + (double)femalesAge/females);

        // Random Question
        // Get the details of highest paid employee in the organization?

        /*
        System.out.println("\nRandom Question:-");

        Employee detailsOfHighestPaid = new Employee();
        double maxSal = 0;
        for (Employee person: employees) {
            Employee emp = (Employee)person;

            if (emp.getSalary() > maxSal) {
                maxSal = emp.getSalary();
                detailsOfHighestPaid = (Employee)person;
            }
        }

        System.out.println("Details of highest paid person:-");
        System.out.println(detailsOfHighestPaid);
        */


        // Q 34
        // Get the names of all employees who have joined after 2015?
        System.out.println("\nQuestion 34:-");

        System.out.println("Names of employees who joined after 2015 are:-");
        for (Employee person: employees) {
            Employee emp = (Employee)person;

            if (emp.getYearOfJoining() > 2015) {
                System.out.println(emp.getName());
            }
        }

        // Q35
        // Count the number of employees in each department?
        System.out.println("\nQuestion 35");

        // Q38
        // Who has the most working experience in the organization?
        System.out.println("\nQuestion 38:-");

        Optional<Employee> mostExperienced = employees.stream()
                .sorted((e1, e2) -> e1.getYearOfJoining() - e2.getYearOfJoining()).findFirst();

        System.out.println(mostExperienced.get());

        // Q39
        // How many male and female employees are there in the sales and marketing team?
        System.out.println("\nQuestion 39:-");

        int malesInSalesAndMarketing = 0, femalesInSalesAndMarketing = 0;
        for (Employee person: employees) {
            // System.out.println(person);
            Employee emp = (Employee)person;

            if (emp.getDepartment() == "Sales And Marketing") {
                if (emp.getGender() == "Male") {
                    malesInSalesAndMarketing += 1;
                }
                else if (emp.getGender() == "Female"){
                    femalesInSalesAndMarketing += 1;
                }
            }
        }

        System.out.println("Number of Male employees in Sales and Marketing = " + malesInSalesAndMarketing);
        System.out.println("Number of Female employees in Sales and Marketing= " + femalesInSalesAndMarketing);


        // Q40
        // What is the average salary of male and female employees?
        System.out.println("\nQuestion 40:-");

        int malesSalary = 0, femalesSalary = 0;
        for (Employee person: employees) {
            Employee emp = (Employee)person;

            if (emp.getGender() == "Male") {
                malesSalary += emp.getSalary();
            }
            else if (emp.getGender() == "Female"){
                femalesSalary += emp.getSalary();
            }
        }

        System.out.println("Average Salary of Male employees = " + (double)malesSalary/males);
        System.out.println("Average Salary of Female employees = " + (double)femalesSalary/females);

        // Q42
        // What is the average salary and total salary of the whole organization?
        System.out.println("\nQuestion 42:-");

        int totalSalary = 0, empCount = 0;
        for (Employee person: employees) {
            Employee emp = (Employee)person;

            totalSalary += emp.getSalary();
            empCount += 1;
        }

        System.out.println("Average salary of the organization = " + (double)totalSalary / empCount);

        // 43
        // Separate the employees who are younger or equal to 25 years from those employees who are older than 25 years?
        System.out.println("\nQuestion 43:-");

        List<String> youngerThan25 = new ArrayList<>();
        List<String> olderThan25 = new ArrayList<>();
        for (Employee person: employees) {
            if (person.getAge() > 25) {
                olderThan25.add(person.getName());
            }
            else {
                youngerThan25.add(person.getName());
            }
        }

        System.out.println("Employees Younger than 25 are: " + youngerThan25);
        System.out.println("Employees Older than 25 are: " + olderThan25);

        // Q44
        // Who is the oldest employee in the organization? What is his age and which department he belongs to?
        System.out.println("\nQuestion 44:-");

        Optional<Employee> oldest = employees.stream()
                .sorted((e1, e2) -> e2.getAge() - e1.getAge()).findFirst();

        System.out.println("Details of oldest employee: " + oldest.get());
    }
}

class Circle {
    private double radius;
    private double area;

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        area = 3.14 * radius * radius;
        return area;
    }
}

public class KT_Day1_Q8 {
    public static void main(String[] args) {
        Circle obj = new Circle();
        System.out.println("8. How would you encapsulate the following class:");
        obj.setRadius(12.7);
        System.out.println(obj.getArea());
    }
}
